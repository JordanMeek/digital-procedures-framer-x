import * as React from "react"
import { Override, Data } from "framer"

const appState = Data({
    selected: null,
    selectedName: null,
})

//let bottom = props.children[0].props.children[0].props

export function Item(props): Override {
    const { id } = props
    const isSelected = appState.selected === id
    const cellName = props.name

    return {
        variants: {
            closed: {
                height: 50,
            },
            open: {
                height: props.height,
            },
        },
        initial: "closed",
        animate: isSelected ? "open" : "closed",
        onTap: () => {
            if (isSelected) {
                appState.selected = null
                appState.selectedName = null
            } else {
                appState.selected = id
                appState.selectedName = props.name
            }
        },
    }
}

export function OpenExtraInfoWithButtons(props): Override {
    const isSelected = appState.selected
    const isSelectedName = appState.selectedName

    console.log(props.name)
    console.log(isSelectedName)

    return {
        variants: {
            closed: {
                //visible: false,
                opacity: 0,
                transition: { duration: 0 },
            },
            open: {
                //visible: true,
                opacity: 1,
                transition: { duration: 0 },
            },
        },
        initial: "closed",
        animate: isSelectedName === props.name ? "open" : "closed",
    }
}
