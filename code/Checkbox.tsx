import * as React from "react"
import { Frame, addPropertyControls, ControlType } from "framer"
import { CheckedIcon } from "./canvas"
import { UncheckedIcon } from "./canvas"

// Open Preview: Command + P
// Learn more: https://framer.com/api

export function Checkbox(props) {
    const { checked, ...rest } = props

    const [isChecked, setIsChecked] = React.useState(checked)

    React.useEffect(() => {
        setIsChecked(checked)
    }, [checked])

    const toggleIsChecked = () => {
        setIsChecked(!isChecked)
    }

    return (
        <Frame
            name="TouchTarget"
            background="none"
            {...rest}
            onTap={toggleIsChecked}
        >
            <UncheckedIcon center />
            {isChecked && <CheckedIcon center />}
        </Frame>
    )
}

Checkbox.defaultProps = {
    checked: false,
    width: 24,
    height: 24,
}

addPropertyControls(Checkbox, {
    checked: {
        title: "Checked",
        type: ControlType.Boolean,
        defaultValue: true,
    },
})
