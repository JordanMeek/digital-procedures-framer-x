import { Override, Data } from "framer"

const appState = Data({
    opacityILS: 1,
    opacityLOC: 0,
    opacityRNAV: 0,
    opacityILSMap: 1,
    opacityRNAVMap: 0,
})

export function FinalSensorSelection(): Override {
    return {
        onValueChange(option, index) {
            appState.opacityILS = 0
            appState.opacityLOC = 0
            appState.opacityRNAV = 0
            appState.opacityILSMap = 0
            appState.opacityRNAVMap = 0

            if (index === 0) {
                appState.opacityILS = 1
                appState.opacityILSMap = 1
            } else if (index === 1) {
                appState.opacityLOC = 1
                appState.opacityILSMap = 1
            } else if (index === 2) {
                appState.opacityRNAV = 1
                appState.opacityRNAVMap = 1
            }
        },
    }
}

export function ChangeToILS(): Override {
    return {
        animate: { opacity: appState.opacityILS },
        trasition: { duration: 0 },
    }
}

export function ChangeToLOC(): Override {
    return {
        animate: { opacity: appState.opacityLOC },
        trasition: { duration: 0 },
    }
}

export function ChangeToRNAV(): Override {
    return {
        animate: { opacity: appState.opacityRNAV },
        trasition: { duration: 0 },
    }
}

export function ChangeToILSMap(): Override {
    return {
        animate: { opacity: appState.opacityILSMap },
        trasition: { duration: 0 },
    }
}

export function ChangeToRNAVMap(): Override {
    return {
        animate: { opacity: appState.opacityRNAVMap },
        trasition: { duration: 0 },
    }
}
