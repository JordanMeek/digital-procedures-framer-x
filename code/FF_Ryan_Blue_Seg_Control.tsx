import * as React from "react"
import {
    Frame,
    FrameProps,
    addPropertyControls,
    ControlType,
    useAnimation,
} from "framer"

type Props = Partial<FrameProps> &
    Partial<{
        setCurrentBy: boolean
        currentIndex: number
        currentOption: string
        onValueChange: (option: string, index: number) => void
    }> & {
        options: string[]
    }

export function SegmentedControl(props: Props) {
    const {
        width,
        height,
        setCurrentBy,
        currentIndex,
        options,
        currentOption,
        onValueChange,
        ...rest
    } = props

    let initialIndex
    if (setCurrentBy === true) {
        initialIndex = currentIndex
    } else {
        initialIndex = options.indexOf(currentOption)
    }
    const [index, setIndex] = React.useState(initialIndex)

    const selectionIndicator = useAnimation()

    let separatorAnimations = []
    for (let i = 0; i < 4; i++) {
        const animation = useAnimation()
        separatorAnimations.push(animation)
    }

    const widthSegment =
        ((width as number) - (options.length - 1)) / options.length

    let separators = []
    for (let i = 0; i < options.length - 1; i++) {
        let opacity = 0.3
        if (i === index - 1 || i === index) {
            opacity = 0
        }
        separators.push(
            <Frame
                name="Separator"
                key={`${props.id}_separator_${i}`}
                width={1}
                height={15}
                background="#8E8E93"
                radius={0.5}
                center="y"
                left={(i + 1) * widthSegment + i}
                animate={separatorAnimations[i]}
                initial={{ opacity: opacity }}
            />
        )
    }

    const changeSelectedIndex = newIndex => {
        selectionIndicator.start({ x: newIndex * widthSegment + newIndex * 1 })
        separatorAnimations.map((separatorAnimation, i) => {
            if (i === newIndex - 1 || i === newIndex) {
                separatorAnimation.start({ opacity: 0 })
            } else {
                separatorAnimation.start({ opacity: 0.3 })
            }
        })
        onValueChange(options[newIndex], newIndex)
        setIndex(newIndex)
    }

    const handleTap = newIndex => {
        changeSelectedIndex(newIndex)
    }

    React.useEffect(() => {
        let newIndex
        if (setCurrentBy === true) {
            newIndex = currentIndex
        } else {
            newIndex = options.indexOf(currentOption)
        }
        changeSelectedIndex(newIndex)
    }, [options, setCurrentBy, currentIndex, currentOption])

    return (
        <Frame
            {...rest}
            width={width}
            height={29}
            background="#2B3645"
            radius={8.91}
            overflow="hidden"
        >
            {separators}
            <Frame
                width={widthSegment - 4}
                height={25.5}
                background="#3298DC"
                radius={6.93}
                center="y"
                left={2}
                shadow="0px 3px 8px rgba(0,0,0,.12), 0px 3px 1px rgba(0,0,0,.04)"
                animate={selectionIndicator}
                initial={{ x: index * widthSegment + index * 1 }}
                transition={{
                    type: "spring",
                    damping: 20,
                    mass: 0.3,
                }}
            />
            {options.map((option, i) => {
                return (
                    <Frame
                        key={`${props.id}_option_${i}`}
                        width={widthSegment}
                        height="100%"
                        background=""
                        left={i * widthSegment + i * 1}
                        color={`${i === index ? "white" : "#677D90"}`}
                        style={{
                            fontSize: 13,
                            fontWeight: i === index ? 600 : 500,
                        }}
                        onTap={() => handleTap(i)}
                    >
                        {option}
                    </Frame>
                )
            })}
        </Frame>
    )
}

SegmentedControl.defaultProps = {
    width: 343,
    height: 29,
    setCurrentBy: true,
    currentIndex: 0,
    currentOption: "0",
    options: ["0", "1", "2", "3", "4"],
    onValueChange: (option, index) => null,
}

addPropertyControls(SegmentedControl, {
    setCurrentBy: {
        type: ControlType.Boolean,
        title: "Set current",
        enabledTitle: "Index",
        disabledTitle: "Option",
    },
    currentIndex: {
        type: ControlType.Number,
        title: "Current",
        min: 0,
        max: 4,
        displayStepper: true,
        hidden(props) {
            return props.setCurrentBy === false
        },
    },
    options: {
        type: ControlType.Array,
        title: "Options",
        propertyControl: {
            type: ControlType.String,
            placeholder: "Set label",
        },
        maxCount: 5,
    },
    currentOption: {
        type: ControlType.String,
        title: "Current",
        placeholder: "Option",
        hidden(props) {
            return props.setCurrentBy === true
        },
    },
})
