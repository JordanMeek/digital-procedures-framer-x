import { Data, animate, Override, Animatable } from "framer"

const data = Data({
    menuOpen: false,
    right: Animatable(-320),
})

export const menuOverride: Override = () => {
    return {
        right: data.right,
    }
}

const springOptions = { tension: 500, friction: 50 }

const bezierCurve = [0.31, 0.03, 0.23, 0.99]
const duration = 0.5
const bezierOptions = { bezierCurve, duration }

export const toggleMenu: Override = () => {
    return {
        onValueChange: isOn => {
            data.menuOpen = !data.menuOpen
            if (data.menuOpen) {
                // Open menu
                // data.left.set(0);
                // animate.spring(data.left, 0, springOptions);
                animate(data.right, 0)
            } else {
                // Close menu
                // data.left.set(-200);
                // animate.spring(data.left, -200, springOptions);
                animate(data.right, -320)
            }
        },
    }
}
