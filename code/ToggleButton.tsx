import * as React from "react"
import { Frame, addPropertyControls, ControlType } from "framer"
import { SidebarIcon } from "./canvas"

export function ToggleButton(props) {
    const [state, setState] = React.useState({
        isOn: props.isOn,
    })

    React.useEffect(() => {
        if (state.isOn !== props.isOn) {
            setState({
                isOn: props.isOn,
            })
        }
    }, [props.isOn])

    const toggleButton = () => {
        props.onValueChange(!state.isOn)
        setState({
            isOn: !state.isOn,
        })
    }

    return (
        <Frame
            height={32}
            width={41}
            radius={2}
            onTap={toggleButton}
            variants={{
                on: { background: "#0E1925" },
                off: { background: "#3498DB" },
            }}
            initial={state.isOn ? "off" : "on"}
            animate={state.isOn ? "off" : "on"}
        >
            <SidebarIcon center />
        </Frame>
    )
}

ToggleButton.defaultProps = {
    isOn: false,
    width: 41,
    height: 32,
    onValueChange: (isOn: boolean) => null,
}

addPropertyControls(ToggleButton, {
    isOn: {
        title: "On",
        type: ControlType.Boolean,
        defaultValue: true,
    },
})
