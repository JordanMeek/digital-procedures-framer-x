import * as React from "react"

const style: React.CSSProperties = {
    height: "100%",
    width: "100%",
}

export class MenuContainer extends React.Component {
    render() {
        return <div style={style}>{this.props.children}</div>
    }
}
