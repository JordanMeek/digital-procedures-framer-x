// WARNING: this file is auto generated, any changes will be lost
import { createDesignComponent, CanvasStore } from "framer"
const canvas = CanvasStore.shared(); // CANVAS_DATA;
export const AddButton = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_k0Fin95BC", {}, 41,32);
export const AddIcon = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_ZmrGgA7N6", {}, 24,24);
export const AirportIcon = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_RbYBIVqZi", {}, 24,24);
export const AirwayIcon = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_mxKahrCab", {}, 24,24);
export const ApproachIcon = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_aRvym6xZF", {}, 24,24);
export const Approach_Info = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_IwbOih8lv", {}, 320,66);
export const ArriveIcon = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_zqyIQHgnK", {}, 24,24);
export const Back_Button = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Text?:string}>(canvas, "id_bZxx9UO2gImnGkyHqvJWpHwl7Ie", {Text:"string"}, 47,32);
export const Buttons = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Frequencies?:string,Frequencies?:string}>(canvas, "id_wjK338M22", {Frequencies:"string",Frequencies:"string"}, 320,44);
export const Cell_Pill = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Pill?:string}>(canvas, "id_L3FGhWj_l", {Pill:"string"}, 44,19);
export const Center_Button = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_LIWoryhFk", {}, 44,44);
export const CheckedIcon = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_XBhMYygDt", {}, 24,24);
export const ChecklistIcon = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_lR5c22dYv", {}, 24,24);
export const Chevron = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_xa9Eka3aa", {}, 8,13);
export const ChevronIcon = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_VWjrIDRxj", {}, 24,24);
export const Close_Header = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Approach?:string,Airport?:string,Text?:string}>(canvas, "id_ImnGkyHqv", {Approach:"string",Airport:"string",Text:"string"}, 320,44);
export const Constraints_Header = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Approach?:string,Text?:string,Text?:string}>(canvas, "id_bZxx9UO2g", {Approach:"string",Text:"string",Text:"string"}, 320,44);
export const Data_Entry_Table_Cell = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Value?:string,Label_Primary?:string}>(canvas, "id_cjyaRqYwt", {Value:"string",Label_Primary:"string"}, 320,44);
export const DepartureIcon = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_BZVno33Cs", {}, 24,24);
export const DiamondIcon = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_CKOgwHYcX", {}, 24,24);
export const FAFIcon = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_YpmOEI3NT", {}, 24,24);
export const FPL_Button_Right = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_m9fB5IVAf", {}, 100,45);
export const Filter_Summary = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Summary?:string,SECTION_NAME?:string}>(canvas, "id_Mh0czLBuo", {Summary:"string",SECTION_NAME:"string"}, 320,35);
export const Filter_Summary_Centered = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Summary?:string}>(canvas, "id_XDaf4Z2R5", {Summary:"string"}, 320,35);
export const FixCell = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Fix1?:string,ALT?:string}>(canvas, "id_hkqkW8CYx", {Fix1:"string",ALT:"string"}, 320,44);
export const FixCell_v2 = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,ALT?:string,Fix1?:string}>(canvas, "id_pL51eqp3k", {ALT:"string",Fix1:"string"}, 320,44);
export const FlightAircraftIcon = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_cykRAMK85", {}, 12,12);
export const FlightCell = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,DateTime?:string,Depature_Airport?:string,Destination_Airport?:string,AircraftID?:string,FlightStatus?:string}>(canvas, "id_R2ddC9DJv", {DateTime:"string",Depature_Airport:"string",Destination_Airport:"string",AircraftID:"string",FlightStatus:"string"}, 320,105);
export const FlightRouteIcon = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_jxtqpFsoF", {}, 12,12);
export const FlightTimeIcon = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_F4PXa46TL", {}, 12,12);
export const Flights_Header = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Approach?:string,Text?:string}>(canvas, "id_OQdHxsMpn", {Approach:"string",Text:"string"}, 320,44);
export const Footer = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Footer?:string}>(canvas, "id_akmb7J6YJ", {Footer:"string"}, 320,35);
export const Four_Buttons = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Frequencies?:string,Frequencies?:string,Frequencies?:string,Frequencies?:string}>(canvas, "id_HfMk4UaHO", {Frequencies:"string",Frequencies:"string",Frequencies:"string",Frequencies:"string"}, 320,44);
export const Header = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Approach?:string,Airport?:string,Text?:string}>(canvas, "id_JIrqvQaEx", {Approach:"string",Airport:"string",Text:"string"}, 320,44);
export const Header_Button = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Text?:string}>(canvas, "id_QWbPs_w8t", {Text:"string"}, 47,32);
export const HoldIcon = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_qK7dhUij2", {}, 24,24);
export const IAFIcon = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_wRINlwxKv", {}, 24,24);
export const IFIcon = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_YbGaKGgJc", {}, 24,24);
export const Inline_Table_Cell_Button = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_hLhPnV0L_", {}, 65,29);
export const KPWM_Wind = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_Uq1s8OYdE", {}, 110,110);
export const Large_CTA_Button = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Text?:string}>(canvas, "id_kj1oMm1Rc", {Text:"string"}, 250,51);
export const Leg_Cell = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Mag_Course?:string,Dot?:string,Distance?:string,Dot?:string,Name?:string}>(canvas, "id_Ff44OHI1P", {Mag_Course:"string",Dot:"string",Distance:"string",Dot:"string",Name:"string"}, 320,13);
export const LoopIcon = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_n2aicy_Fr", {}, 24,24);
export const Minimum_Cell = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,VIS_Value?:string,VIS?:string,DA_Value?:string,DA?:string,DH_Value?:string,DH?:string,Type?:string,Restriction?:string}>(canvas, "id_d2qvtz69H", {VIS_Value:"string",VIS:"string",DA_Value:"string",DA:"string",DH_Value:"string",DH:"string",Type:"string",Restriction:"string"}, 320,63);
export const Minimum_Cell_without_Restriction = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,VIS_Value?:string,VIS?:string,DA_Value?:string,DA?:string,DH_Value?:string,DH?:string,Type?:string}>(canvas, "id_jTKgtZlR6", {VIS_Value:"string",VIS:"string",DA_Value:"string",DA:"string",DH_Value:"string",DH:"string",Type:"string"}, 320,63);
export const Missed_Cell = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Leg_Name?:string,Mag_Course?:string,Fix1?:string,Fix2?:string,ALT?:string}>(canvas, "id_NSg9Xz8kG", {Leg_Name:"string",Mag_Course:"string",Fix1:"string",Fix2:"string",ALT:"string"}, 320,39);
export const Navlog_Cell_v1 = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Leg_Name?:string,Mag_Course?:string,Fix1?:string,Fix2?:string,DIS?:string,TIME?:string,ALT?:string}>(canvas, "id_ago6kHWMf", {Leg_Name:"string",Mag_Course:"string",Fix1:"string",Fix2:"string",DIS:"string",TIME:"string",ALT:"string"}, 320,39);
export const Navlog_Cell_v3 = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Fix1?:string,Leg_Name?:string,Mag_Course?:string,ALT?:string,TIME?:string,DIS?:string}>(canvas, "id_r8LcU2Z8Y", {Fix1:"string",Leg_Name:"string",Mag_Course:"string",ALT:"string",TIME:"string",DIS:"string"}, 320,44);
export const Notes = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_IcNeDZaQh", {}, 320,91);
export const One_Flight_Header = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Text?:string,DeparturetoDestination?:string,DateTime?:string}>(canvas, "id_xzSQR61aL", {Text:"string",DeparturetoDestination:"string",DateTime:"string"}, 320,44);
export const One_Line_Push_Table_Cell = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Value?:string,Label_Primary?:string}>(canvas, "id_W__lSy3qi", {Value:"string",Label_Primary:"string"}, 320,44);
export const Option_Check_Cell = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Label?:string}>(canvas, "id_nB_1U2HUO", {Label:"string"}, 320,44);
export const PerformanceStats = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,dist?:string,ete?:string,eta?:string,fuel?:string,wind?:string}>(canvas, "id_VpCOweP8x", {dist:"string",ete:"string",eta:"string",fuel:"string",wind:"string"}, 320,56);
export const ReorderIcon = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_Jw6_Qm8zO", {}, 24,24);
export const Runway_Selection_Cell = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Label_Primary?:string,Pill?:string,Pill?:string}>(canvas, "id_OmOGbn51K", {Label_Primary:"string",Pill:"string",Pill:"string"}, 250,63);
export const Search_Box = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_xN1WjgZQR", {}, 320,44);
export const Section_Header = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,SECTION_NAME?:string,DIS?:string,TIME?:string,ALT?:string}>(canvas, "id_NqWSOZfkK", {SECTION_NAME:"string",DIS:"string",TIME:"string",ALT:"string"}, 250,35);
export const Selected_Minimum = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,VIS_Value?:string,VIS?:string,DA_Value?:string,DA?:string,DH_Value?:string,DH?:string,Type?:string,Type?:string,Restriction?:string,MINIMUM_TERPS_?:string}>(canvas, "id_droA9QVWZ", {VIS_Value:"string",VIS:"string",DA_Value:"string",DA:"string",DH_Value:"string",DH:"string",Type:"string",Type:"string",Restriction:"string",MINIMUM_TERPS_:"string"}, 320,64);
export const SidebarIcon = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_i1KyANJ_n", {}, 24,24);
export const Sidebar_Button_Off = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_qVTg1VJrH", {}, 41,32);
export const Sidebar_Button_On = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_IRJbZgF1A", {}, 41,32);
export const Single_Button_Cell = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_t6IyQWP9P", {}, 320,44);
export const Single_Cell_Button = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Frequencies?:string}>(canvas, "id_yJ2PqhSSA", {Frequencies:"string"}, 320,44);
export const Two_Line_Optional_Check_Table_Cell = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Label_Primary?:string,Label_Secondary?:string}>(canvas, "id_gPHG08HKV", {Label_Primary:"string",Label_Secondary:"string"}, 320,63);
export const Two_Line_Push_Table_Cell = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,Value?:string,Label_Primary?:string,Label_Secondary?:string}>(canvas, "id_DCFTbn1Oy", {Value:"string",Label_Primary:"string",Label_Secondary:"string"}, 320,63);
export const UncheckedIcon = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_FJ8JRbu4g", {}, 24,24);
export const VORIcon = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_xMTPeM6kK", {}, 24,24);
export const WaypointIcon = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_oinCHe4Ta", {}, 24,24);
export const Weather_Widget = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string,$29_94_inHg?:string,$4_C_70_?:string,$043_?:string,$18_kts?:string,BKN_500_OVC_600?:string,$1sm?:string,light_rain_mist?:string,$043_?:string,$18_kts?:string,$043_?:string}>(canvas, "id_flhwu5TM0", {$29_94_inHg:"string",$4_C_70_:"string",$043_:"string",$18_kts:"string",BKN_500_OVC_600:"string",$1sm:"string",light_rain_mist:"string",$043_:"string",$18_kts:"string",$043_:"string"}, 320,84);

export const colors = Object.freeze({
    /** #7BC34E */
    "Green": "var(--token-e2fbe286-db1f-4f2b-956c-149e4c8158c1, rgb(123, 195, 78))",
    /** #132231 */
    "Cell Background": "var(--token-e6c999f9-5c87-478e-a04d-d1e8c506a07f, rgb(19, 34, 49))",
    /** #FFFFFF */
    "White": "var(--token-cae25551-b3b5-4b93-bb6a-c498a38e404a, rgb(255, 255, 255))",
    /** #000000 */
    "Black": "var(--token-3777dbf3-502e-4484-a1d9-f02a187de9c9, rgb(0, 0, 0))",
    /** rgba(255, 255, 255, 0.5) */
    "White50": "var(--token-3bad315a-2986-44ba-9dda-d6d8d93ac61a, rgba(255, 255, 255, 0.5))",
    /** #0D1A25 */
    "Arrow Background": "var(--token-769f8449-6d19-4371-b90c-98e46d620f92, rgb(13, 26, 37))",
    /** #16283A */
    "Spacer": "var(--token-9180a9d0-8cf7-4372-9755-8852a196f986, rgb(22, 40, 58))",
    /** #0E1925 */
    "Section Background": "var(--token-2a6245d5-1281-4d13-a400-04f6b5d5f7d7, rgb(14, 25, 37))",
    /** #697D90 */
    "Section Text": "var(--token-2b1a48a7-7118-4553-b646-8811cbb07d25, rgb(105, 125, 144))",
    /** #424954 */
    "Runway Background": "var(--token-e9008e4f-5ce8-4458-bd02-ec3ebca3febe, rgb(66, 73, 84))",
    /** #9EABB8 */
    "Secondary Text": "var(--token-df07d1aa-a6ac-4477-90d5-0fa1bc1979f1, rgb(158, 171, 184))",
    /** #2C3845 */
    "Shadow": "var(--token-9db0ac22-6d73-43b3-9a97-67b7ee2e85e9, rgb(44, 56, 69))",
    /** #7D509E */
    "Frequencies": "var(--token-7bf608fb-0811-4da3-97bc-c0aa7487afa7, rgb(125, 80, 158))",
    /** #4FC9EC */
    "Plate": "var(--token-4866cf58-81ef-4993-a344-64240d624f37, rgb(79, 201, 236))",
    /** #3498DB */
    "Ryan Blue": "var(--token-59ee47bc-072c-485b-96cf-eb0372058d2d, rgb(52, 152, 219))",
    /** #1D3750 */
    "Brand": "var(--token-0453608f-cabb-410a-b201-de37fc960a26, rgb(29, 55, 80))",
    /** #1E6DD8 */
    "Current Weather": "var(--token-383bba9f-9318-4982-aa67-631a126af0cd, rgb(30, 109, 216))",
    /** #697D90 */
    "Non Editable Values": "var(--token-200fa439-068f-40e2-8870-666a06eafca9, rgb(105, 125, 144))",
    /** rgba(107, 126, 140, 0.1) */
    "Pill Backround": "var(--token-1c04b967-404e-44ac-af15-4c2a9a24eca3, rgba(107, 126, 140, 0.1))",
    /** #122330 */
    "Search Box": "var(--token-049321b6-d196-48d0-a819-9f9eceeb6d45, rgb(18, 35, 48))",
    /** rgba(0, 0, 0, 0.5) */
    "Black50": "var(--token-48d5020a-7306-4ab2-be4b-d48d24810324, rgba(0, 0, 0, 0.5))",
    /** #84BDFF */
    "FPLAirportBlue": "var(--token-842ace57-862f-4696-bac1-5e764fe00e70, rgb(132, 189, 255))",
    /** #B2DAB4 */
    "FPLSIDSTAR": "var(--token-8c380f7a-84d9-4562-8df4-7224852d7fb5, rgb(178, 218, 180))",
    /** #212F40 */
    "FPLAirportBlue2": "var(--token-ffd40859-0a96-4cb4-bd8a-f8f59c8212a0, rgb(33, 47, 64))",
    /** #FE3824 */
    "Cancel": "var(--token-e36399bd-26d0-42d5-970a-04a7037cc62f, rgb(254, 56, 36))",
    /** #3498DB */
    "Accent 1": "var(--token-40a8a0b0-2382-4c4d-8072-79604396d6bf, rgb(52, 152, 219))",
    /** #FF40FF */
    "Accent 2": "var(--token-fe52c42f-0594-4b03-8ce7-43aa5c36a1b1, rgb(255, 64, 255))",
    /** #0E1925 */
    "Base": "var(--token-9b741fd1-ab1e-469e-9ef3-63aa61257b47, rgb(14, 25, 37))",
    /** #FF9500 */
    "Warning": "var(--token-91dd6f1b-bcaf-4726-a9b5-09b452a0a401, rgb(255, 149, 0))",
    /** #0076CC */
    "AccentDS": "var(--token-3434d041-6f4f-4084-847a-e8884b9d7cab, rgb(0, 118, 204))",
    /** #FCC48A */
    "FPLAirway": "var(--token-52591393-3528-4be1-94b5-21062d1111e4, rgb(252, 196, 138))",
    /** #979EDC */
    "FPLFix": "var(--token-5c5e6073-7783-4078-9479-3651f223d2d0, rgb(151, 158, 220))",
    /** #8ACBCC */
    "FPLApproach": "var(--token-615a90c9-7a27-4787-828b-ede54b3d8786, rgb(138, 203, 204))",
    /** #CCD1D2 */
    "FPLTrafficPattern": "var(--token-ae0c0be5-d18b-4cf8-96c8-064f67698322, rgb(204, 209, 210))",
})
