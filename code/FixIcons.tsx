import * as React from "react"
import { Frame, addPropertyControls, ControlType } from "framer"
import {
    WaypointIcon,
    FAFIcon,
    IAFIcon,
    IFIcon,
    VORIcon,
    HoldIcon,
    AirportIcon,
    AirwayIcon,
    DepartureIcon,
    ArriveIcon,
    ApproachIcon,
    ChecklistIcon,
} from "./canvas"

export function FixIcon(props) {
    const { value = "a", ...rest } = props
    const iconType = {
        a: "WaypointIcon",
        b: "IAFIcon",
        c: "IFIcon",
        d: "FAFIcon",
        e: "VORIcon",
        f: "HoldIcon",
        g: "AirportIcon",
        h: "AirwayIcon",
        i: "DepartureIcon",
        j: "ArriveIcon",
        k: "ApproachIcon",
        l: "ChecklistIcon",
    }

    return (
        <Frame {...rest}>
            {iconType[value] === "WaypointIcon" && <WaypointIcon />}
            {iconType[value] === "IAFIcon" && <IAFIcon />}
            {iconType[value] === "IFIcon" && <IFIcon />}
            {iconType[value] === "FAFIcon" && <FAFIcon />}
            {iconType[value] === "VORIcon" && <VORIcon />}
            {iconType[value] === "HoldIcon" && <HoldIcon />}
            {iconType[value] === "AirportIcon" && <AirportIcon />}
            {iconType[value] === "AirwayIcon" && <AirwayIcon />}
            {iconType[value] === "DepartureIcon" && <DepartureIcon />}
            {iconType[value] === "ArriveIcon" && <ArriveIcon />}
            {iconType[value] === "ApproachIcon" && <ApproachIcon />}
            {iconType[value] === "ChecklistIcon" && <ChecklistIcon />}
        </Frame>
    )
}

addPropertyControls(FixIcon, {
    value: {
        type: ControlType.Enum,
        defaultValue: "a",
        options: ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l"],
        optionTitles: [
            "Waypoint",
            "IAF",
            "IF",
            "FAF",
            "VOR",
            "Hold",
            "Airport",
            "Airway",
            "Departure",
            "Arrival",
            "Approach",
            "Checklist",
        ],
        title: "Fix Type",
    },
})

FixIcon.defaultProps = {
    height: 24,
    width: 24,
    background: "none",
}
