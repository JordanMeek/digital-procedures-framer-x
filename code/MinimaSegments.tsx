import { Override, Data } from "framer"

const appState = Data({
    opacityCatAScrollFull: 1,
    opacityCatAScrollTDZorCLout: 0,
    opacityCatAScrollALout: 0,
    opacityCatBScrollFull: 0,
    opacityCatBScrollTDZorCLout: 0,
    opacityCatBScrollALout: 0,
    opacityCatCScrollFull: 0,
    opacityCatCScrollTDZorCLout: 0,
    opacityCatCScrollALout: 0,
    opacityCatDScrollFull: 0,
    opacityCatDScrollTDZorCLout: 0,
    opacityCatDScrollALout: 0,
})

const segState = Data({
    category: 0,
    approachLights: 0,
})

const segOption = Data({
    category: "Cat A",
    approachLights: "Full ALS",
})

export function CatSegControl(): Override {
    return {
        onValueChange(option, index) {
            ClearOutAllMinimaScrollViews()
            segState.category = index
            segOption.category = option
            ChangeSelectedMinima()
        },
    }
}

export function ALSSegControl(): Override {
    return {
        onValueChange(option, index) {
            ClearOutAllMinimaScrollViews()
            segState.approachLights = index
            segOption.approachLights = option
            ChangeSelectedMinima()
        },
    }
}

function ChangeSelectedMinima() {
    if (segState.category === 0 && segState.approachLights === 0) {
        appState.opacityCatAScrollFull = 1
    } else if (segState.category === 0 && segState.approachLights === 1) {
        appState.opacityCatAScrollTDZorCLout = 1
    } else if (segState.category === 0 && segState.approachLights === 2) {
        appState.opacityCatAScrollALout = 1
    } else if (segState.category === 1 && segState.approachLights === 0) {
        appState.opacityCatBScrollFull = 1
    } else if (segState.category === 1 && segState.approachLights === 1) {
        appState.opacityCatBScrollTDZorCLout = 1
    } else if (segState.category === 1 && segState.approachLights === 2) {
        appState.opacityCatBScrollALout = 1
    } else if (segState.category === 2 && segState.approachLights === 0) {
        appState.opacityCatCScrollFull = 1
    } else if (segState.category === 2 && segState.approachLights === 1) {
        appState.opacityCatCScrollTDZorCLout = 1
    } else if (segState.category === 2 && segState.approachLights === 2) {
        appState.opacityCatCScrollALout = 1
    } else if (segState.category === 3 && segState.approachLights === 0) {
        appState.opacityCatDScrollFull = 1
    } else if (segState.category === 3 && segState.approachLights === 1) {
        appState.opacityCatDScrollTDZorCLout = 1
    } else if (segState.category === 3 && segState.approachLights === 2) {
        appState.opacityCatDScrollALout = 1
    }
    console.log(segState.category, segState.approachLights)
}

function ClearOutAllMinimaScrollViews() {
    appState.opacityCatAScrollFull = 0
    appState.opacityCatAScrollTDZorCLout = 0
    appState.opacityCatAScrollALout = 0
    appState.opacityCatBScrollFull = 0
    appState.opacityCatBScrollTDZorCLout = 0
    appState.opacityCatBScrollALout = 0
    appState.opacityCatCScrollFull = 0
    appState.opacityCatCScrollTDZorCLout = 0
    appState.opacityCatCScrollALout = 0
    appState.opacityCatDScrollFull = 0
    appState.opacityCatDScrollTDZorCLout = 0
    appState.opacityCatDScrollALout = 0
}

// Cat A
export function CatAScrollFull(): Override {
    return {
        animate: { opacity: appState.opacityCatAScrollFull },
        trasition: { duration: 0 },
    }
}

export function CatAScrollTDZorCLout(): Override {
    return {
        animate: { opacity: appState.opacityCatAScrollTDZorCLout },
        trasition: { duration: 0 },
    }
}

export function CatAScrollALout(): Override {
    return {
        animate: { opacity: appState.opacityCatAScrollALout },
        trasition: { duration: 0 },
    }
}

// Cat B
export function CatBScrollFull(): Override {
    return {
        animate: { opacity: appState.opacityCatBScrollFull },
        trasition: { duration: 0 },
    }
}

export function CatBScrollTDZorCLout(): Override {
    return {
        animate: { opacity: appState.opacityCatBScrollTDZorCLout },
        trasition: { duration: 0 },
    }
}

export function CatBScrollALout(): Override {
    return {
        animate: { opacity: appState.opacityCatBScrollALout },
        trasition: { duration: 0 },
    }
}

// Cat C
export function CatCScrollFull(): Override {
    return {
        animate: { opacity: appState.opacityCatCScrollFull },
        trasition: { duration: 0 },
    }
}

export function CatCScrollTDZorCLout(): Override {
    return {
        animate: { opacity: appState.opacityCatCScrollTDZorCLout },
        trasition: { duration: 0 },
    }
}

export function CatCScrollALout(): Override {
    return {
        animate: { opacity: appState.opacityCatCScrollALout },
        trasition: { duration: 0 },
    }
}

// Cat D
export function CatDScrollFull(): Override {
    return {
        animate: { opacity: appState.opacityCatDScrollFull },
        trasition: { duration: 0 },
    }
}

export function CatDScrollTDZorCLout(): Override {
    return {
        animate: { opacity: appState.opacityCatDScrollTDZorCLout },
        trasition: { duration: 0 },
    }
}

export function CatDScrollALout(): Override {
    return {
        animate: { opacity: appState.opacityCatDScrollALout },
        trasition: { duration: 0 },
    }
}
